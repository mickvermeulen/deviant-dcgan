# Deviant DCGAN

This repository aims to create [DCGAN model](https://arxiv.org/abs/1511.06434) that can produce Deviant Art images by using arbitrary Deviant Art search terms to create a data set and training a DCGAN on that data using TensorFlow.

It's recommended to setup GPU accelartion for TensorFlow: GANs have a vast amount of parameters so training them on a CPU can take incredibly long.

NOTE: As the data returned is generally of quite small volume, the model is prone to overfitting. Either way, it's a fun proof of concept.

### Requirements

- Pyton (>3.7)

### Setup

This is the regular virtualenv installation setup, this can easily be mirrored to use conda or some other environment manager. I for example used the [Apple Miniforge](https://developer.apple.com/metal/tensorflow-plugin/) environment to allow for Metal GPU accelaration for model training.

```bash
python -m venv venv
. venv/bin/activate
pip install -r requirements.txt
```

### Running

Running this toolkit is split into two parts: running the data miner and then training the model, both of them are implemented as notebooks. You can easily start either of them using the Jupyterlab interface:

```bash
jupyter notebook
```

Then either open `data.ipynb` or `net.ipynb` and follow the instructions in the notebook.
